//
//  DemoUtility.swift
//  mySpark
//
//  Created by Guest on 1/10/19.
//  Copyright © 2019 blah. All rights reserved.
//

import DJISDK



func djiHeadingToDegrees(heading: Double) -> Double {
    var theHeading: Double = 0.0
    
    if heading == -180 {
        theHeading = 180
    } else if heading == 0 {
        theHeading = 0.0
    } else if heading < 0 {
        theHeading = 180.0 + (180.0 - abs(heading))
    } else {
        theHeading = heading
    }
    
    return theHeading
}


func DEGREE(_ x: Double) -> Double {
    
    return Double((x)*180 / .pi)
}
func RADIAN(_ x: Double) -> Double {
    return Double((x) * .pi / 180)
}



//@inline(__always) func ShowMessage(title: String?, message: String?, target: Any?, cancleBtnTitle: String?) {
//    DispatchQueue.main.async(execute: {
//        let alert = UIAlertView(title: title, message: message, delegate: target, cancelButtonTitle: cancleBtnTitle, otherButtonTitles: "")
//        alert.show()
//    })
//}

class DemoUtility {
    class func fetchFlightController() -> DJIFlightController? {
        if !(DJISDKManager.product() != nil) {
            return nil
        }
        
        if (DJISDKManager.product() is DJIAircraft) {
            return (DJISDKManager.product() as? DJIAircraft)?.flightController
        }
        
        return nil
    }
}

