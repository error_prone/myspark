//
//  CompassView.swift
//  CompassControl
//
//  Created by Guest on 1/6/19.
//  Copyright © 2019 blah. All rights reserved.
//

import UIKit

class CompassView: UIView {
    
    var radius: CGFloat = 120
    var largeStrokeDistance: CGFloat = 15
    var smallStrokeDistance: CGFloat = 5

    
//     Only override draw() if you perform custom drawing.
//     An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        //print("Draw method has been called")
        
        guard let currentContext = UIGraphicsGetCurrentContext() else {
            print("Could not get the context")
            return
        }
                drawIt(using: currentContext)
    }
    
    func drawCompass() {
        
        setNeedsDisplay()
    }
    
    private func drawIt(using context: CGContext){
        let centerPoint = CGPoint(x: bounds.size.width/2, y: bounds.size.height/2)
        let startStroke = CGPoint(x: centerPoint.x, y: centerPoint.y - 120)
        let endStroke = CGPoint(x: centerPoint.x, y: centerPoint.y - 120 + largeStrokeDistance)
        
        context.move(to: startStroke)
        context.addLine(to: endStroke)
        context.setLineCap(.square)
        context.setLineWidth(3.0)
        context.setStrokeColor(UIColor.lightGray.cgColor)
        context.strokePath()
        context.addArc(center: centerPoint, radius: 120, startAngle: CGFloat(0).degreesToRadians, endAngle: CGFloat(360).degreesToRadians, clockwise: true)
        context.strokePath()
        
        secondMarkers(context: context, x: centerPoint.x, y: centerPoint.y, radius: radius, sides: 8, color: UIColor.lightGray)
        drawText(context: context, x: centerPoint.x, y: centerPoint.y, radius: radius, sides: 8, color: UIColor.lightGray)
  
    }

    func degree2radian(a:CGFloat)->CGFloat {
        let b = CGFloat(Double.pi) * a/180
        return b
    }
 
    private func circleCircumferencePoints(sides:Int, x:CGFloat, y:CGFloat, radius:CGFloat, adjustment:CGFloat=0)->[CGPoint] {
        let angle = degree2radian(a: 360/CGFloat(sides))
        let cx = x // x origin
        let cy = y // y origin
        let r  = radius // radius of circle
        var i = sides
        var points = [CGPoint]()
        while points.count <= sides {
            let xpo = cx - r * cos(angle * CGFloat(i)+degree2radian(a: adjustment))
            let ypo = cy - r * sin(angle * CGFloat(i)+degree2radian(a: adjustment))
            points.append(CGPoint(x: xpo, y: ypo))
            i -= 1;
        }
        return points
    }
    
    private func secondMarkers(context:CGContext, x:CGFloat, y:CGFloat, radius:CGFloat, sides:Int, color:UIColor) {
        // retrieve points
        let points = circleCircumferencePoints(sides: sides,x: x,y: y,radius: radius)
        // create path
        let path = CGMutablePath()
        // determine length of marker as a fraction of the total radius
        var divider:CGFloat = 1/16
        
        for (index,p) in points.enumerated() {
            if index % 2 == 0 {
                divider = 1/8
            }
            else {
                divider = 1/16
            }
            
            let xn = p.x + divider*(x-p.x)
            let yn = p.y + divider*(y-p.y)
            // build path
            path.move(to: CGPoint(x: p.x, y: p.y))
            path.addLine(to: CGPoint(x: xn, y: yn))
            path.closeSubpath()
            // add path to context
            context.addPath(path)
        }
        // set path color
        let cgcolor = color.cgColor
        context.setStrokeColor(cgcolor)
        context.setLineWidth(3.0)
        context.strokePath()
        
    }
    
    private func drawText(context: CGContext, x: CGFloat, y: CGFloat, radius: CGFloat, sides: Int, color: UIColor) {
        context.textMatrix = .identity
        context.translateBy(x: x, y: y)
        context.scaleBy(x: 1.0, y: -1.0)
        context.rotate(by: CGFloat(0).degreesToRadians)
        
        let strings = ["N", "E", "S", "W"]
        
        for str in strings {
            let str = str
            let font = UIFont.systemFont(ofSize: 20)
            let attributes: [NSAttributedString.Key: Any] = [
                .font: font,
                .foregroundColor: UIColor.lightGray,
                ]
            
            let attrString = NSAttributedString(string: str, attributes: attributes)
            let attribStringWidth = attrString.size().width
            let path = CGMutablePath()
            path.addRect(CGRect(x: -attribStringWidth/2, y: 80 , width: attribStringWidth, height: 24))
            
            let framesetter = CTFramesetterCreateWithAttributedString(attrString as CFAttributedString)
            let frame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, attrString.length), path, nil)
            CTFrameDraw(frame, context)
            context.rotate(by: CGFloat(-90).degreesToRadians)
            
        }
    }
    
}

extension FloatingPoint {
    var degreesToRadians: Self {return self * .pi / 180}
}

