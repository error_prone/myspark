//
//  MissionController.swift
//  mySpark
//
//  Created by Guest on 1/11/19.
//  Copyright © 2019 blah. All rights reserved.
//

import DJISDK
import UIKit

class MissionController {
    
    
    private var missionState = DJIWaypointMissionState.unknown {
        
        didSet { stateDidChange()  }
    }
    
    func missionOperator() -> DJIWaypointMissionOperator? {
        return DJISDKManager.missionControl()!.waypointMissionOperator()
    }
    
    private let notificationCenter: NotificationCenter
    
    init(notificationCenter: NotificationCenter = .default) {
        self.notificationCenter = notificationCenter
    }
    
    
    
    func stateDidChange(){
        
    }
    
    func test(){
        
        
        let mission = DJIMutableWaypointMission()
        mission.maxFlightSpeed = 15
        mission.autoFlightSpeed = 8
        mission.finishedAction = .noAction
        mission.headingMode = .auto
        mission.flightPathMode = .normal
        mission.rotateGimbalPitch = true
        mission.exitMissionOnRCSignalLost = true
        mission.gotoFirstWaypointMode = .pointToPoint
        mission.repeatTimes = 1
        
        
        let loc1 = CLLocationCoordinate2DMake(31.0, -102.0)
        let waypoint1 = DJIWaypoint(coordinate: loc1)
        waypoint1.altitude = 25
        waypoint1.heading = 0
        waypoint1.actionRepeatTimes = 1
        waypoint1.actionTimeoutInSeconds = 60
        waypoint1.cornerRadiusInMeters = 5
        waypoint1.turnMode = .clockwise
        waypoint1.gimbalPitch = 0
        let wpAction = DJIWaypointAction(actionType: .stay, param: 3000)
        waypoint1.add(wpAction)
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
