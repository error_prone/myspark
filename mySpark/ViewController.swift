//
//  ViewController.swift
//  mySpark
//
//  Created by Guest on 1/3/19.
//  Copyright © 2019 blah. All rights reserved.
//


import UIKit
import DJISDK
import DJIWidget
import MessageUI



class ViewController: UIViewController, CLLocationManagerDelegate, DJIFlightControllerDelegate {
    
    var mainView = MainView()
    
    let locationDelegate = LocationDelegate()
    
    var isRecording : Bool!
    var locationManager: CLLocationManager?
    var userLocation: CLLocationCoordinate2D?
    var droneLocation: CLLocationCoordinate2D?
    var waypointMission: DJIMutableWaypointMission?
    private var cHeading: Double?
    private var cLat: CLLocationDegrees?
    private var cLong: CLLocationDegrees?
    
    var aFlightController: DJIFlightController? = nil

    override func loadView() {
        view = mainView
    }
 
    //MARK: - METHODS...
    //MARK: -
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
        
//        let camera = self.fetchCamera()
//        if((camera != nil) && (camera?.delegate?.isEqual(self))!){
//            camera?.delegate = nil
//        }
//        self.resetVideoPreview()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager?.delegate = locationDelegate
        self.startUpdateLocation()
        
        
        
        //DJISDKManager.registerApp(with: self)
        //recordTimeLabel.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    

    
    // MARK: - DJIFlightControllerDelegate
    func flightController(_ fc: DJIFlightController, didUpdate state: DJIFlightControllerState) {
        let headingInDegrees =  djiHeadingToDegrees(heading: fc.compass?.heading ?? 0.0)
//        self.cHeading = headingInDegrees
        let currentLocation = state.aircraftLocation
//        self.cLat = currentLocation?.coordinate.latitude
//        self.cLong = currentLocation?.coordinate.longitude
        //print(DJICompass().heading)
        //print(state.attitude.yaw)
        //print("Heading: \(fc.compass?.heading), \(currentLocation?.coordinate.latitude), \(currentLocation?.coordinate.longitude)")
        
        
    }

    
    func missionOperator() -> DJIWaypointMissionOperator? {
        return DJISDKManager.missionControl()!.waypointMissionOperator()
    }
    
    func startUpdateLocation() {
        if CLLocationManager.locationServicesEnabled() {
            if locationManager == nil {
                locationManager = CLLocationManager()
                locationManager?.delegate = self
                locationManager?.desiredAccuracy = kCLLocationAccuracyBest
                locationManager?.distanceFilter = CLLocationDistance(0.1)
                locationManager?.requestAlwaysAuthorization()
                locationManager?.startUpdatingLocation()
                

    //            locationManager = CLLocationManager()
    //            if let aLocationManager = locationManager {
    //                aLocationManager.delegate = self
    //            }
    //            locationManager.delegate = self
    //            locationManager.desiredAccuracy = kCLLocationAccuracyBest
    //            locationManager.distanceFilter = CLLocationDistance(0.1)
    //            if locationManager.responds(to: #selector(self.requestAlwaysAuthorization)) {
    //                locationManager.requestAlwaysAuthorization()
    //            }
    //            locationManager.startUpdatingLocation()
            }
        } else {
            //ShowMessage("Location Service is not available", "", nil, "OK")
        }
    }
    
}
