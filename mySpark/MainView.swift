//
//  MainView.swift
//  CompassControl
//
//  Created by Guest on 1/16/19.
//  Copyright © 2019 blah. All rights reserved.
//

import UIKit

class MainView: UIView {
    let compassView: UIView = CompassView()
    let fpv: UIView = FPV()
    var statemanager: StateManager?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        viewSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        viewSetup()
    }
    
    func viewSetup(){
        statemanager = StateManager.sharedInstance
        statemanager?.aPI = aircraftPositionInfo
        // code from ViewController.swift "viewDidLoad" func, goes HERE
        self.backgroundColor = UIColor.white
        
        fpv.frame = CGRect(x: 0, y: 270, width: 270, height: 270)
        fpv.backgroundColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.20)
        addSubview(fpv)
        
        compassView.frame = CGRect(x: 0, y: 0, width: 270, height: 270)
        compassView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(compassView)
    }
    
    func aircraftPositionInfo(_ aircraftPositionInfo: aircraftPositionInfo) -> () {
        print(aircraftPositionInfo.aircraftHeading)
    }

}
