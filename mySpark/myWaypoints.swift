//
//  myWaypoints.swift
//  mySpark
//
//  Created by Guest on 1/13/19.
//  Copyright © 2019 blah. All rights reserved.
//

import Foundation
import DJISDK


class MYWaypoints {
    let wp1 = CLLocationCoordinate2D(latitude: 31.84611432, longitude: -102.46396739)
    let wp2 = CLLocationCoordinate2D(latitude: 31.84603505, longitude: -102.46451117)
    let wp3 = CLLocationCoordinate2D(latitude: 31.84609186, longitude: -102.46415876)
    let home = CLLocationCoordinate2D(latitude: 31.846163746, longitude: -102.46416717)
}
