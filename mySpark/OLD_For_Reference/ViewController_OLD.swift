
//
//  ViewController.swift
//  mySpark
//
//  Created by Guest on 1/3/19.
//  Copyright © 2019 blah. All rights reserved.
//


import UIKit
import DJISDK
import DJIWidget
import MessageUI



class ViewController: UIViewController, CLLocationManagerDelegate, DJIFlightControllerDelegate {
    
    var mainView = MainView()
    
    let locationDelegate = LocationDelegate()
    
    var isRecording : Bool!
    var locationManager: CLLocationManager?
    var userLocation: CLLocationCoordinate2D?
    var droneLocation: CLLocationCoordinate2D?
    var waypointMission: DJIMutableWaypointMission?
    private var cHeading: Double?
    private var cLat: CLLocationDegrees?
    private var cLong: CLLocationDegrees?
    
    var aFlightController: DJIFlightController? = nil
    
    
    
    
    
    
    
    
    override func loadView() {
        view = mainView
    }
    
    
    //    @IBAction func captureCoordinatesBtnPush(_ sender: Any) {
    //        if  let heading = self.cHeading,
    //            let lat = self.cLat,
    //            let long = self.cLong {
    //            self.headingLabel.text = String(heading)
    //            self.latLabel.text = String(lat)
    //            self.longLabel.text = String(long)
    //        }
    //
    //    }
    
    //MARK: - METHODS...
    //MARK: -
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.startUpdateLocation()
        
        //        let camera = self.fetchCamera()
        //        if((camera != nil) && (camera?.delegate?.isEqual(self))!){
        //            camera?.delegate = nil
        //        }
        //        self.resetVideoPreview()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager?.delegate = locationDelegate
        
        
        
        //DJISDKManager.registerApp(with: self)
        //recordTimeLabel.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    //    func formatSeconds(seconds: UInt) -> String {
    //        let date = Date(timeIntervalSince1970: TimeInterval(seconds))
    //
    //        let dateFormatter = DateFormatter()
    //        dateFormatter.dateFormat = "mm:ss"
    //        return(dateFormatter.string(from: date))
    //    }
    
    //    func showAlertViewWithTitle(title: String, withMessage message: String) {
    //
    //        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    //        let okAction = UIAlertAction.init(title:"OK", style: UIAlertAction.Style.default, handler: nil)
    //        alert.addAction(okAction)
    //        self.present(alert, animated: true, completion: nil)
    //
    //    }
    
    //MARK: - DJISDKManagerDelegate Methods
    //    func productConnected(_ product: DJIBaseProduct?) {
    //
    //        NSLog("Product Connected")
    //
    //        if (product != nil) {
    //            self.aFlightController = DemoUtility.fetchFlightController()
    //            self.aFlightController?.delegate = self
    //            NotificationCenter.default.post(name: Notification.Name.init("ProductConnected"), object: self)
    //            //self.setupVideoPreviewer()
    //        }
    //
    //        //If this demo is used in China, it's required to login to your DJI account to activate the application. Also you need to use DJI Go app to bind the aircraft to your DJI account. For more details, please check this demo's tutorial.
    //        DJISDKManager.userAccountManager().logIntoDJIUserAccount(withAuthorizationRequired: false) { (state, error) in
    //            if(error != nil){
    //                NSLog("Login failed: %@" + String(describing: error))
    //            }
    //        }
    //
    //    }
    
    // MARK: - DJIFlightControllerDelegate
    func flightController(_ fc: DJIFlightController, didUpdate state: DJIFlightControllerState) {
        let headingInDegrees =  djiHeadingToDegrees(heading: fc.compass?.heading ?? 0.0)
        //        self.cHeading = headingInDegrees
        let currentLocation = state.aircraftLocation
        //        self.cLat = currentLocation?.coordinate.latitude
        //        self.cLong = currentLocation?.coordinate.longitude
        //print(DJICompass().heading)
        //print(state.attitude.yaw)
        //print("Heading: \(fc.compass?.heading), \(currentLocation?.coordinate.latitude), \(currentLocation?.coordinate.longitude)")
        
        
    }
    //
    //    @IBAction func onTakeoffButtonClicked(_ sender: Any) {
    //        let fc: DJIFlightController? = DemoUtility.fetchFlightController()
    //        if fc != nil {
    //            fc?.startTakeoff(completion: {  error in
    //                if error != nil {
    //                    print("Takeoff command not successful...")
    //                } else {
    //                    print("Takeoff Success!!!")
    //                }
    //            })
    //        } else {
    //            print("Takeoff FAIL!!!")
    //        }
    //    }
    
    //    @IBAction func onLandButtonClicked(_ sender: Any) {
    //        let fc: DJIFlightController? = DemoUtility.fetchFlightController()
    //        if fc != nil {
    //            fc?.startLanding (completion: {  error in
    //                if error != nil {
    //                    print("Landing command not successful...")
    //                } else {
    //                    print("Takeoff Command,  Success!!!")
    //                }
    //            })
    //        } else {
    //            print("Landing FAIL!!!")
    //        }
    //
    //    }
    
    //    func productDisconnected() {
    //
    //        NSLog("Product Disconnected")
    //
    ////        let camera = self.fetchCamera()
    ////        if((camera != nil) && (camera?.delegate?.isEqual(self))!){
    ////            camera?.delegate = nil
    ////        }
    ////        self.resetVideoPreview()
    //    }
    
    //    func appRegisteredWithError(_ error: Error?) {
    //
    //        var message = "Register App Successed!"
    //        if (error != nil) {
    //            message = "Register app failed! Please enter your app key and check the network."
    //        } else {
    //            DJISDKManager.startConnectionToProduct()
    //        }
    //
    //        //self.showAlertViewWithTitle(title:"Register App", withMessage: message)
    //    }
    
    
    //MARK: - IBAction Methods
    //    @IBAction func captureAction(_ sender: UIButton) {
    
    //        let camera = self.fetchCamera()
    //        if (camera != nil) {
    //            camera?.setMode(DJICameraMode.shootPhoto, withCompletion: {(error) in
    //                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1){
    //                    camera?.startShootPhoto(completion: { (error) in
    //                        if (error != nil) {
    //                            NSLog("Shoot Photo Error: " + String(describing: error))
    //                        }
    //                    })
    //                }
    //            })
    //        }
    //    }
    //    @IBAction func doMission2(_ sender: Any) {
    //
    //        let prod = DemoUtility.fetchFlightController()
    //        prod?.setNoviceModeEnabled(false, withCompletion: nil)
    //        prod?.setMaxFlightHeight(50, withCompletion: nil)
    //        prod?.setMaxFlightRadius(75, withCompletion: nil)
    //        DJISDKManager.missionControl()?.stopTimeline()
    //        DJISDKManager.missionControl()?.unscheduleEverything()
    //
    //        let mywpts = MYWaypoints()
    //
    //        let mission = DJIMutableWaypointMission()
    //        mission.maxFlightSpeed = 15
    //        mission.autoFlightSpeed = 5
    //        mission.finishedAction = .noAction
    //        mission.headingMode = .auto
    //        mission.flightPathMode = .normal
    //        mission.rotateGimbalPitch = true
    //        mission.exitMissionOnRCSignalLost = true
    //        mission.gotoFirstWaypointMode = .pointToPoint
    //        mission.repeatTimes = 1
    //
    //
    //
    //        //let wpAction = DJIWaypointAction(actionType: .stay, param: 3000)
    //
    //
    //        let loc1 = mywpts.wp1
    //        let waypoint1 = DJIWaypoint(coordinate: loc1)
    //        waypoint1.altitude = 9
    //        waypoint1.heading = 0
    //        waypoint1.speed = 5
    //        waypoint1.actionRepeatTimes = 1
    //        waypoint1.actionTimeoutInSeconds = 60
    //        waypoint1.cornerRadiusInMeters = 5
    //        waypoint1.turnMode = .clockwise
    //        waypoint1.gimbalPitch = 0
    //        //waypoint1.add(wpAction)
    //
    //        let loc2 = mywpts.wp2
    //        let waypoint2 = DJIWaypoint(coordinate: loc2)
    //        waypoint2.altitude = 25
    //        waypoint2.heading = 0
    //        waypoint2.speed = 15
    //        waypoint2.actionRepeatTimes = 1
    //        waypoint2.actionTimeoutInSeconds = 60
    //        waypoint2.cornerRadiusInMeters = 5
    //        waypoint2.turnMode = .clockwise
    //        waypoint2.gimbalPitch = 0
    //        //waypoint2.add(wpAction)
    //
    //        let loc3 = mywpts.wp3
    //        let waypoint3 = DJIWaypoint(coordinate: loc3)
    //        waypoint3.altitude = 5
    //        waypoint3.heading = 0
    //        waypoint3.speed = 10
    //        waypoint3.actionRepeatTimes = 1
    //        waypoint3.actionTimeoutInSeconds = 60
    //        waypoint3.cornerRadiusInMeters = 5
    //        waypoint3.turnMode = .clockwise
    //        waypoint3.gimbalPitch = 0
    //        //waypoint2.add(wpAction)
    //
    //        let loc4 = mywpts.home
    //        let waypoint4 = DJIWaypoint(coordinate: loc4)
    //        waypoint4.altitude = 5
    //        waypoint4.heading = 0
    //        waypoint4.speed = 2
    //        waypoint4.actionRepeatTimes = 1
    //        waypoint4.actionTimeoutInSeconds = 60
    //        waypoint4.cornerRadiusInMeters = 5
    //        waypoint4.turnMode = .counterClockwise
    //        waypoint4.gimbalPitch = 0
    //
    //        mission.add(waypoint1)
    //        mission.add(waypoint2)
    //        mission.add(waypoint3)
    //        mission.add(waypoint4)
    //
    //        mission.checkParameters()
    //
    //        var elements = [DJIMissionControlTimelineElement]()
    //        //elements.append(DJITakeOffAction())
    //        elements.append(mission)
    //
    //        //elements.append(DJIGoHomeAction())
    //        DJISDKManager.missionControl()?.scheduleElements(elements)
    //        DJISDKManager.missionControl()?.startTimeline()
    //
    //    }
    
    //    @IBAction func doMission(_ sender: Any) {
    //
    //        let prod = DemoUtility.fetchFlightController()
    //        prod?.setNoviceModeEnabled(false, withCompletion: nil)
    //        prod?.setMaxFlightHeight(50, withCompletion: nil)
    //        prod?.setMaxFlightRadius(75, withCompletion: nil)
    //        DJISDKManager.missionControl()?.stopTimeline()
    //        DJISDKManager.missionControl()?.unscheduleEverything()
    //
    //        //let mywpts = MYWaypoints()
    //
    //        let mission = DJIMutableWaypointMission()
    //        mission.maxFlightSpeed = 5
    //        mission.autoFlightSpeed = 2
    //        mission.finishedAction = .noAction
    //        mission.headingMode = .usingWaypointHeading
    //        mission.flightPathMode = .normal
    //        mission.rotateGimbalPitch = true
    //        mission.exitMissionOnRCSignalLost = true
    //        mission.gotoFirstWaypointMode = .pointToPoint
    //        mission.repeatTimes = 1
    //
    //
    //
    //        //let wpAction = DJIWaypointAction(actionType: .stay, param: 3000)
    //
    //
    //        //let loc1 = mywpts.wp1
    //        let waypoint1 = DJIWaypoint(coordinate: CLLocationCoordinate2D(latitude: 31.846182474677068, longitude: -102.46401998261474))
    //        waypoint1.altitude = 9
    //        waypoint1.heading = 153
    //        waypoint1.speed = 1
    //        waypoint1.actionRepeatTimes = 1
    //        waypoint1.actionTimeoutInSeconds = 60
    //        waypoint1.cornerRadiusInMeters = 5
    //        waypoint1.turnMode = .clockwise
    //        waypoint1.gimbalPitch = -60
    //        //waypoint1.add(wpAction)
    //
    //        //let loc2 = mywpts.wp2
    //        let waypoint2 = DJIWaypoint(coordinate: CLLocationCoordinate2D(latitude: 31.846155073423144, longitude: -102.46408327709419))
    //        waypoint2.altitude = 9
    //        waypoint2.heading = -173
    //        waypoint2.speed = 1
    //        waypoint2.actionRepeatTimes = 1
    //        waypoint2.actionTimeoutInSeconds = 60
    //        waypoint2.cornerRadiusInMeters = 5
    //        waypoint2.turnMode = .clockwise
    //        waypoint2.gimbalPitch = -60
    //        //waypoint2.add(wpAction)
    //
    //        //let loc3 = mywpts.wp3
    //        let waypoint3 = DJIWaypoint(coordinate: CLLocationCoordinate2D(latitude: 31.846153033139146, longitude: -102.46408678427518))
    //        waypoint3.altitude = 9
    //        waypoint3.heading = 141
    //        waypoint3.speed = 1
    //        waypoint3.actionRepeatTimes = 1
    //        waypoint3.actionTimeoutInSeconds = 60
    //        waypoint3.cornerRadiusInMeters = 5
    //        waypoint3.turnMode = .clockwise
    //        waypoint3.gimbalPitch = -60
    //        //waypoint2.add(wpAction)
    //
    //        //let loc4 = mywpts.home
    //        let waypoint4 = DJIWaypoint(coordinate: CLLocationCoordinate2D(latitude: 31.846137747065622, longitude: -102.46414159881864))
    //        waypoint4.altitude = 9
    //        waypoint4.heading = 0
    //        waypoint4.speed = 1
    //        waypoint4.actionRepeatTimes = 1
    //        waypoint4.actionTimeoutInSeconds = 60
    //        waypoint4.cornerRadiusInMeters = 5
    //        waypoint4.turnMode = .counterClockwise
    //        waypoint4.gimbalPitch = -60
    //
    //        mission.add(waypoint1)
    //        mission.add(waypoint2)
    //        mission.add(waypoint3)
    //        mission.add(waypoint4)
    //
    //        mission.checkParameters()
    //
    //        var elements = [DJIMissionControlTimelineElement]()
    //        //elements.append(DJITakeOffAction())
    //        elements.append(mission)
    //
    //        //elements.append(DJIGoHomeAction())
    //        DJISDKManager.missionControl()?.scheduleElements(elements)
    //        DJISDKManager.missionControl()?.startTimeline()
    //
    //    }
    
    //    @IBAction func recordAction(_ sender: UIButton) {
    
    //        let camera = self.fetchCamera()
    //        if (camera != nil) {
    //            if (self.isRecording) {
    //                camera?.stopRecordVideo(completion: { (error) in
    //                    if (error != nil) {
    //                        NSLog("Stop Record Video Error: " + String(describing: error))
    //                    }
    //                })
    //            } else {
    //                camera?.startRecordVideo(completion: { (error) in
    //                    if (error != nil) {
    //                        NSLog("Start Record Video Error: " + String(describing: error))
    //                    }
    //                })
    //            }
    //        }
    //    }
    
    //    @IBAction func workModeSegmentChange(_ sender: UISegmentedControl) {
    
    //        let camera = self.fetchCamera()
    //        if (camera != nil) {
    //            if (sender.selectedSegmentIndex == 0) {
    //                camera?.setMode(DJICameraMode.shootPhoto,  withCompletion: { (error) in
    //                    if (error != nil) {
    //                        NSLog("Set ShootPhoto Mode Error: " + String(describing: error))
    //                    }
    //                })
    //
    //            } else if (sender.selectedSegmentIndex == 1) {
    //                camera?.setMode(DJICameraMode.recordVideo,  withCompletion: { (error) in
    //                    if (error != nil) {
    //                        NSLog("Set RecordVideo Mode Error: " + String(describing: error))
    //                    }
    //                })
    //
    //            }
    //        }
    //    }
    
    func missionOperator() -> DJIWaypointMissionOperator? {
        return DJISDKManager.missionControl()!.waypointMissionOperator()
    }
    
    func startUpdateLocation() {
        if CLLocationManager.locationServicesEnabled() {
            if locationManager == nil {
                locationManager = CLLocationManager()
                locationManager?.delegate = self
                locationManager?.desiredAccuracy = kCLLocationAccuracyBest
                locationManager?.distanceFilter = CLLocationDistance(0.1)
                locationManager?.requestAlwaysAuthorization()
                locationManager?.startUpdatingLocation()
                
                
                //            locationManager = CLLocationManager()
                //            if let aLocationManager = locationManager {
                //                aLocationManager.delegate = self
                //            }
                //            locationManager.delegate = self
                //            locationManager.desiredAccuracy = kCLLocationAccuracyBest
                //            locationManager.distanceFilter = CLLocationDistance(0.1)
                //            if locationManager.responds(to: #selector(self.requestAlwaysAuthorization)) {
                //                locationManager.requestAlwaysAuthorization()
                //            }
                //            locationManager.startUpdatingLocation()
            }
        } else {
            //ShowMessage("Location Service is not available", "", nil, "OK")
        }
    }
    
}
