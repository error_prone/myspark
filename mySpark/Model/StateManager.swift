//
//  StateManager.swift
//  mySpark
//
//  Created by Guest on 1/19/19.
//  Copyright © 2019 blah. All rights reserved.
//

import Foundation
import DJISDK

class StateManager: NSObject, DJIFlightControllerDelegate {
    static let sharedInstance = StateManager()
    var aFlightController: DJIFlightController? = nil
    
    var aPI: ((_ aircraftPositionInfo: aircraftPositionInfo) -> ())?
    
    private var prodIsConnected: Bool = false
    private var prodHeading: Double = 0.0
    private var prodLat: Double = 90.0
    private var prodLong: Double = 0.0
    private var prodAlt: Double = 0.0

    private override init(){
        super.init()
        //self.aFlightController?.delegate = self
        let _ = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { [unowned self] timer in
            if self.prodIsConnected {
                if self.aFlightController == nil {
                    self.aFlightController = (DJISDKManager.product() as? DJIAircraft)?.flightController
                    self.aFlightController?.delegate = self
                }
                self.timerFired()
            }
        }
    }
    
    func setProductConnectionStatus(_ prodConnectionState: Bool) {
        self.prodIsConnected = prodConnectionState
    }
    
    private func timerFired() {
        let anAircraftPositionInfo = aircraftPositionInfo(aircraftHeading: prodHeading, aircraftLat: prodLat, aircraftLong: prodLong, aircraftAlt: prodAlt)
        aPI?(anAircraftPositionInfo)
        
    }
    
    func flightController(_ fc: DJIFlightController, didUpdate state: DJIFlightControllerState) {
        self.prodHeading =  djiHeadingToDegrees(heading: fc.compass?.heading ?? 0.0)
        self.prodAlt = state.altitude
        let currentLocation = state.aircraftLocation
        self.prodLat = (currentLocation?.coordinate.latitude) ?? 0
        self.prodLong = (currentLocation?.coordinate.longitude) ?? 0
        
    }
}

struct aircraftPositionInfo {
    var aircraftHeading: Double
    var aircraftLat: Double
    var aircraftLong: Double
    var aircraftAlt: Double
}
