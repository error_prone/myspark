//
//  ProductCommunicationManager.swift
//  mySpark
//
//  Created by Guest on 1/3/19.
//  Copyright © 2019 blah. All rights reserved.
//

import UIKit
import DJISDK

class ProductCommunicationManager: NSObject {
    let statemanager: StateManager = StateManager.sharedInstance
    
    // Set this value to true to use the app with the Bridge and false to connect directly to the product
    let enableBridgeMode = false
    
    // When enableBridgeMode is set to true, set this value to the IP of your bridge app.
    let bridgeAppIP = "10.81.55.116"
    var aFlightController: DJIFlightController = DJIFlightController()
    
    func registerWithSDK() {
        let appKey = Bundle.main.object(forInfoDictionaryKey: SDK_APP_KEY_INFO_PLIST_KEY) as? String
        
        guard appKey != nil && appKey!.isEmpty == false else {
            NSLog("Please enter your app key in the info.plist")
            return
        }
        
        DJISDKManager.registerApp(with: self)
    }
    
}

extension ProductCommunicationManager : DJISDKManagerDelegate {
    func appRegisteredWithError(_ error: Error?) {
        
        NSLog("SDK Registered with error \(String(describing: error?.localizedDescription))")
        
        if enableBridgeMode {
            DJISDKManager.enableBridgeMode(withBridgeAppIP: bridgeAppIP)
        } else {
            DJISDKManager.startConnectionToProduct()
        }
        
    }
    
    func productConnected(_ product: DJIBaseProduct?) {
        
        print("************************* PRODUCT CONNECTED *************************")
        
        
        if (product != nil) {
            statemanager.setProductConnectionStatus(true)
            //self.aFlightController = DemoUtility.fetchFlightController()!
            //self.aFlightController.delegate = self
            NotificationCenter.default.post(name: Notification.Name.init("ProductConnected"), object: self)
            //self.setupVideoPreviewer()
        }
        
        //If this demo is used in China, it's required to login to your DJI account to activate the application. Also you need to use DJI Go app to bind the aircraft to your DJI account. For more details, please check this demo's tutorial.
        DJISDKManager.userAccountManager().logIntoDJIUserAccount(withAuthorizationRequired: false) { (state, error) in
            if(error != nil){
                NSLog("Login failed: %@" + String(describing: error))
            }
        }
        
    }
    
    func productDisconnected() {
        print("************************* PRODUCT DISCONNECTED *************************")
        statemanager.setProductConnectionStatus(false)

    }
    
    func componentConnected(withKey key: String?, andIndex index: Int) {
        print(key ?? "")
        
    }
    
    func componentDisconnected(withKey key: String?, andIndex index: Int) {
        print(key ?? "")
    }
}
