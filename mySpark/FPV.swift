//
//  FPV.swift
//  mySpark
//
//  Created by Guest on 1/16/19.
//  Copyright © 2019 blah. All rights reserved.
//

import UIKit
import DJISDK
import DJIWidget


class FPV: UIView, DJICameraDelegate, DJIVideoFeedListener{
    
    var theProduct: DJIBaseProduct?  {
        didSet{checkConnected() }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        viewSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        viewSetup()
    }
    
    func viewSetup(){
        NotificationCenter.default.addObserver(self, selector: #selector(checkConnected), name: Notification.Name.init("ProductConnected"), object: nil)
        //checkConnected()
    }
    
    public func setProduct(prod: DJIBaseProduct?){
        self.theProduct = prod
    }
    
    @objc func checkConnected(){
        let product = DJISDKManager.product()
        if (product != nil) {
            let camera = fetchCamera()
            if (camera != nil) {
                camera!.delegate = self
            }
            self.setupVideoPreviewer()
        }
    }
    
    func fetchCamera() -> DJICamera? {
        let product = DJISDKManager.product()
      
        if (product == nil) {
            return nil
        }
        
        if (product!.isKind(of: DJIAircraft.self)) {
            return (product as! DJIAircraft).camera
        } else if (product!.isKind(of: DJIHandheld.self)) {
            return (product as! DJIHandheld).camera
        }
        return nil
    }
    
    func setupVideoPreviewer() {
        DJIVideoPreviewer.instance().setView(self)
        let product = DJISDKManager.product()
    
        //Use "SecondaryVideoFeed" if the DJI Product is A3, N3, Matrice 600, or Matrice 600 Pro, otherwise, use "primaryVideoFeed".
        if ((product?.model == DJIAircraftModelNameA3)
            || (product?.model == DJIAircraftModelNameN3)
            || (product?.model == DJIAircraftModelNameMatrice600)
            || (product?.model == DJIAircraftModelNameMatrice600Pro)){
            DJISDKManager.videoFeeder()?.secondaryVideoFeed.add(self, with: nil)
        }else{
            DJISDKManager.videoFeeder()?.primaryVideoFeed.add(self, with: nil)
        }
        DJIVideoPreviewer.instance().start()
    }
    
    func resetVideoPreview() {
        DJIVideoPreviewer.instance().unSetView()
        let product = DJISDKManager.product()
        //Use "SecondaryVideoFeed" if the DJI Product is A3, N3, Matrice 600, or Matrice 600 Pro, otherwise, use "primaryVideoFeed".
        if ((product?.model == DJIAircraftModelNameA3)
            || (product?.model == DJIAircraftModelNameN3)
            || (product?.model == DJIAircraftModelNameMatrice600)
            || (product?.model == DJIAircraftModelNameMatrice600Pro)){
            DJISDKManager.videoFeeder()?.secondaryVideoFeed.remove(self)
        }else{
            DJISDKManager.videoFeeder()?.primaryVideoFeed.remove(self)
        }
    }
    
    //MARK: -  DJICameraDelegate Method
    func camera(_ camera: DJICamera, didUpdate cameraState: DJICameraSystemState) {
//        self.isRecording = cameraState.isRecording
//        self.recordTimeLabel.isHidden = !self.isRecording
        
//        self.recordTimeLabel.text = formatSeconds(seconds: cameraState.currentVideoRecordingTimeInSeconds)
        
//        if (self.isRecording == true) {
//            self.recordButton.setTitle("Stop Record", for: UIControl.State.normal)
//        } else {
//            self.recordButton.setTitle("Start Record", for: UIControl.State.normal)
//        }
        
        //Update UISegmented Control's State
        if (cameraState.mode == DJICameraMode.shootPhoto) {
//            self.workModeSegmentControl.selectedSegmentIndex = 0
        } else {
//            self.workModeSegmentControl.selectedSegmentIndex = 1
        }
    }
    
    //MARK: - DJIVideoFeedListener Method
    func videoFeed(_ videoFeed: DJIVideoFeed, didUpdateVideoData rawData: Data) {
        let videoData = rawData as NSData
        let videoBuffer = UnsafeMutablePointer<UInt8>.allocate(capacity: videoData.length)
        videoData.getBytes(videoBuffer, length: videoData.length)
        DJIVideoPreviewer.instance().push(videoBuffer, length: Int32(videoData.length))
    }
    

}
